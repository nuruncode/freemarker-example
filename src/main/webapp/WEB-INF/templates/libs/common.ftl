<#-- Macro Example -->

<#macro heading title description = ''>
  <h1>${title}</h1>
  <#if description?has_content>
    ${description}
  </#if>
</#macro>

<#-- Macro with loop and nested -->



<#-- Function -->

<#function findFirst collection>
    <#local firstElement = {} />
    <#if collection?has_content && collection?size gt 0>
      <#local firstElement = collection[0] />
    </#if>
    <#return firstElement />
</#function>
