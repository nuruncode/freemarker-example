<#import "/libs/common.ftl" as cmn />

<html>
  <head>
    <title>TEST</title>
  </head>
  <body>

    <#-- macro example -->
    <@cmn.heading title = "allo" description = "des" />

    <#-- function example -->
    <#assign firstElement = cmn.findFirst(['y', 'b']) />
    ${firstElement}
  </body>

</html>