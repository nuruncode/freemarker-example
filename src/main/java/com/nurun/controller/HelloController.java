package com.nurun.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HelloController {

  public HelloController() {
    System.out.println("HelloController");
  }

  @GetMapping("/test")
  public String home() {
    return "home";
  }

}
